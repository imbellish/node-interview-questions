var axios = require("axios");

const url = "http://localhost:3000"


class http {
    getUserList() {
        return axios.get(url + "/users/userlist").catch(function(error){
            alert("Failed to fetch user list")
        }); 
    }

    deleteUser(id) {
        return axios.delete(url + "/users/deleteuser/" + id).catch(function(error){
            alert("Failed to delete user")
        });
    }

    addUser(data){
        return axios.post(url + "/users/adduser/", data).catch(function(error){
            alert("Failed to add user")
        });
    }

    updateUser(data){
        return axios.put(url + "/users/updateuser/" + data._id, data).catch(function(error){
            alert("Failed to update user")
        })
    }

}

export default http;