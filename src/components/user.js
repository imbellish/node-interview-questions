import React from "react";
import http from "../http";
import TextButton from "./common";

const api = new http();

class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false,
        }
        this.deleteUser = this.deleteUser.bind(this);
        this.initEditUser = this.initEditUser.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.closeEditor = this.closeEditor.bind(this);
    }

    deleteUser()  {
        console.log("Deleting user " + this.props._id)
        this.setState({editable: false})
        api.deleteUser(this.props._id)
    }

    initEditUser() {
        this.setState({editable: true})
    }

    closeEditor() {
        this.setState({editable: false})
    }

    saveChanges() {
        // interface for *updating* users
        this.setState({editable: false})
        
        api.updateUser({_id: this.props._id,
                        fullname: this.state.fullname,
                        username: this.state.username,
                        email: this.state.email,
                        age: this.state.age,
                        location: this.state.location,
        }).then(function(response){
            console.log("updated user")
        })
    }

    handleInputChange(event){
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    render() {
        if (!this.state.editable){
            return (
                <tr>
                    <td>{this.props.fullname}</td>
                    <td>{this.props.username}</td>
                    <td>{this.props.email}</td>
                    <td>{this.props.age}</td>
                    <td>{this.props.location}</td>
                    <td>
                        <TextButton text="Delete" clickHandler={this.deleteUser}/>
                        <TextButton text="Edit" clickHandler={this.initEditUser}/>
                    </td>
                    <td>{this.props.state}</td>
                </tr>
        )

        } else {
            return (
                <tr>
                    <td>
                        <input name="fullname" defaultValue={this.props.fullname} onChange={this.handleInputChange}></input>
                    </td>
                    <td>
                        <input name="username" defaultValue={this.props.username} onChange={this.handleInputChange}></input>
                    </td>
                    <td>
                        <input name="email" defaultValue={this.props.email} onChange={this.handleInputChange}></input>
                    </td>
                    <td>
                        <input name="age" defaultValue={this.props.age} onChange={this.handleInputChange}></input>
                    </td>
                    <td>
                        <input name="location" defaultValue={this.props.location} onChange={this.handleInputChange}></input>
                    </td>
                    <td>
                        <TextButton text="Cancel" clickHandler={this.closeEditor}/>
                        <TextButton text="Save" clickHandler={this.saveChanges}/>
                    </td>
                    <td>{this.props.state}</td>
                </tr>
            )
        }

    }
}

export default User;