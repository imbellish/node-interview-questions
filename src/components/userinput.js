import React from "react";
import http from "../http";

const api = new http();

class UserForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            _id: null,
            username: null,
            email: null,
            fullname: null,
            age: null,
            location: null,
            gender: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.submit = this.submit.bind(this);
    }

    handleInputChange(event){
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    submit(e){
        // public interface for adding user
        e.preventDefault();
        var that = this;
        api.addUser(this.state).then(function(response){
            // console.log(response);
            that.setState({_id: null, username: null, email: null,
                fullname: null, age: null, location: null, gender: null})
            that.forceUpdate()
        })
    }

    render() {
        return(
            <form>
            <input name="_id" type="hidden" value={this.state._id}></input>
            <div>
            <label>Username:<br></br>
                <input name="username" onChange={this.handleInputChange}></input>
            </label>
            </div>
            <div>
            <label>
                Email:<br></br>
                <input name="email" onChange={this.handleInputChange}></input>
            </label>
            </div>
            <div>
            <label>
                Full name:<br></br>
                <input name="fullname" onChange={this.handleInputChange}></input>
            </label>
            </div>
            <div>
            <label>
                Age:<br></br>
                <input name="age" onChange={this.handleInputChange}></input>
            </label>
            </div>
            <div>
            <label>
                Location:<br></br>
                <input name="location" onChange={this.handleInputChange}></input>
            </label>
            </div>
            <div>
            <label>
                Gender:<br></br>
                <input name="gender" onChange={this.handleInputChange}></input>
            </label>
            </div>
            <button onClick={this.submit}>Confirm</button>
            </form>
            
        )
    }
}

class UserInput extends React.Component {

    // NOTE: this could be merged with the above component and some additional
    // state could be introduced to indicate that the request is in flight

    constructor(props){
        super(props);
        this.state = {
            expanded: false
        }
        this.expand = this.expand.bind(this);
        this.close = this.close.bind(this);
    }

    expand() {
        this.setState({expanded: true})
    }
    close() {
        this.setState({expanded: false})
    }
    render() {
        if (this.state.expanded){
            return (  
                <div>        
                <UserForm />
                <button onClick={this.close}>Cancel</button>
                </div>
            )
        }
        else {
            return(
                <a onClick={this.expand} class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
            )
        }
    }
}

export default UserInput;