import React from "react";
import User from "./user";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import http from "../http";

const api = new http();
const client = new W3CWebSocket('ws://localhost:3000/chatter')

class UserList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    };

    componentWillMount() {
        var that = this; // gross 
        api.getUserList().then(function(response){
            response.data.forEach(user => {
                user.state = 'init';
            });
            that.setState({users: response.data})
            that.forceUpdate();
        });

        client.onmessage = function(message) {
            // process incoming changes made by any client
            var user = JSON.parse(message.data);
            console.log(user);
            var currentUsers = that.state.users;
            if (user.state === 'deleted'){
                // it makes more sense to simply indicate the 
                // user is deleted with a color, otherwise
                // it's terrible UX.
                var removeIndex = currentUsers.map(function(item)
                    {return item._id}).indexOf(user._id);
                currentUsers.splice(removeIndex, 1);
            }
            else if (user.state === 'modified') {
                var updateIndex = currentUsers.map(function(item)
                    {return item._id}).indexOf(user._id);
                currentUsers[updateIndex] = user
            }
            else if (user.state === 'created'){
                currentUsers.push(user)
            }
            
            that.setState({users: currentUsers});
        }
    }

    render() {
        return (
            <div class="collection">
            <table>
                <thead>
                    <th>Full name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Age</th>
                    <th>Location</th>
                    <th>Options</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    {this.state.users.map((user) => <User username={user.username}
                                                    email={user.email}
                                                    fullname={user.fullname}
                                                    age={user.age}
                                                    location={user.location}
                                                    gender={user.gender}
                                                    _id={user._id} 
                                                    state={user.state}/>)}
                </tbody>
            </table>
            </div>
        )
        
    }

}

export default UserList;