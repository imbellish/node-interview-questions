import React from "react";


class TextButton extends React.Component {

    render() {
        return(
            <button onClick={this.props.clickHandler} >{this.props.text}</button>
        )
    }
}

export default TextButton;