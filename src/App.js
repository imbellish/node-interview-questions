import React from "react";
import UserList from "./components/userlist";
import UserInput from "./components/userinput";

class App extends React.Component {

    render() {
        return(
            <div class="container">
            <UserList />
            <UserInput />
            </div>
        )
    }
        
}

export default App;