var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.insert(req.body, {timeout: 3000}, function(err, result){
        if (!err){
            res.status(201).send({msg: 'OK'});
            console.log(result);
            result.state = 'created';
            req.broadcast(result);
        } else {
            // mongodb server down
            res.status(502).send({msg: 'ERROR'})
        }
    });
});

/*
 * PUT to updateuser
 */

 router.put('/updateuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userId = req.params.id;
    var body = req.body;
    collection.findOneAndUpdate({ '_id': userId }, { $set: req.body }, function(err, result){
        if (!err){
            res.status(200).send({msg: 'OK'});
            result.state = 'modified'
            req.broadcast(result);
        } else {
            res.status(502).send({msg: 'ERROR'})
        }

    })

 });

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToDelete = req.params.id;
    collection.remove({ '_id' : userToDelete }, function(err) {
        if (!err){
            res.send({msg: 'OK'})
            req.broadcast({_id: userToDelete, state: 'deleted'})
        } else {
            res.status(502).send({msg: 'ERROR'})
        }
        
    });
});

module.exports = router;