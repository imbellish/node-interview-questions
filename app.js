var express = require("express");
var expressWs = require("express-ws");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
// Database
var monk = require("monk");
var db = monk("localhost:27017/node_interview_question");

var routes = require("./routes/index");
var users = require("./routes/users");

var app = express();
var wsapp = expressWs(app);

// defines a /chatter websocket for pushing changes to the UI
// see the logic in src/component/userlist.js
app.ws('/chatter', function(ws, req){
});

// make the websocket endpoint available to the router
app.use(function(req, res, next) {
  req.broadcast = function(msg) {
    var aWss = wsapp.getWss('/chatter')
    aWss.clients.forEach(function (client){
      client.send(JSON.stringify(msg))
    })
  }
  next();
})

// point express to the proper build directory
app.use(express.static("build"))


app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Make our db accessible to our router
app.use(function(req, res, next) {
  req.db = db;
  next();
});

app.use("/", routes);
app.use("/users", users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

/// error handlers

// Note: I'm not familiar enough with express to determine if I
// should remove these or not. The other .jade views are removed

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {}
  });
});

module.exports = app;
