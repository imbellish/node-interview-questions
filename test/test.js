var monk = require('monk');

// clear out local db before proceeding 
var db = monk('localhost:27017/node_interview_question');
var collection = db.get('userlist');
collection.drop();

// test registry
require('./adduser.js');
require('./updateuser.js');
require('./getusers.js');
require('./deleteuser.js');
